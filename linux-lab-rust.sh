#!/bin/sh
make BOARD=x86_64/pc
cd /labs/linux-lab/src/linux-stable
git remote add linux-next https://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git
# 因为仓库并不是mainline，因此这一步非常巨大，后续必须优化
git fetch linux-next
git fetch --tags linux-next
git checkout -b my_local_branch next-20210526