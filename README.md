# Linux Lab 新增 Rust for Linux 开发支持

1. 项目标题：Linux Lab 新增 Rust for Linux 开发支持
2. 项目描述：Rust for Linux 正在往 Linux 官方主线提交代码，目前有部分代码已经进入 Linux Next，这意味着下一个 Linux 内核版本将正式可以使用 Rust 来开发 Linux 模块。但是，Rust 作为一个新兴语言，对很多人来说都很陌生，第一步的环境搭建就能挡住很多人。本次项目旨在为 Linux Lab 做好 Rust for Linux 的环境准备工作，确保 Linux Lab 用户可以直接上手在 Linux Lab 中用 Rust 编写 Linux 内核模块，并提供必要的上手模块案例和中文文档。
3. 项目难度：高
4. 项目社区导师：[@falcon](https://gitee.com/falcon)
5. 导师联系方式：[falcon@tinylab.org](mailto:falcon@tinylab.org)
6. 合作导师联系方式：[@虎哥](https://gitee.com/mike) Tang, [daogangtang@live.com](mailto:daogangtang@live.com)
7. 项目产出要求：
   - 为 Linux Lab 新增 Rust 开发环境
   - 确保 Rust for Linux 中的模块可以编译与运行
   - 确保 make module 兼容 Rust 撰写的 Linux 内核模块
   - 撰写并发表详细开发与使用文档
   - 可选：使用 Rust 撰写1-2个新的 Linux 内核模块
8. 项目技术要求：
   - 基本的 Linux 命令
   - 熟悉 Makefile 和 Bash
   - 熟悉 Linux Lab
   - Linux 内核与驱动开发基础
   - 具有 Rust 语言使用经验优先
   - 用 Rust 撰写过 Linux 内核模块优先
9. 相关的开源软件仓库列表：
   - Cloud Lab: [Cloud Lab: 云实验室 —— 基于 Docker 的云实验室管理系统，支持 noVNC 远程桌面和 Gateone 远程 SSH 控制台；社区正在试做免安装的 Linux Lab Disk，抢先体验地址：https://shop155917374.taobao.com](https://gitee.com/tinylab/cloud-lab)
   - Markdown Lab: [markdown-lab: Markdown 实验室 —— 提供了制作幻灯、文章、书籍和简历的模板，能轻松生成 pdf 和 html，支持加密和水印。社区正在试做免安装的 Linux Lab Disk，抢先体验地址：https://shop155917374.taobao.com](https://gitee.com/tinylab/markdown-lab)
   - Rust for Linux: [Rust for Linux · GitHub](https://github.com/Rust-for-Linux)
   - Rust: https://www.rust-lang.org/